+++
title = "Die Tarifkommission kann man nur durch Vereinbarung binden"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

Um unsere Position und Meinung in die Tarifkommission zu tragen bzw. sie daran zu binden, steht uns nur die Möglichkeit offen, auf eine (freiwillige) Selbstverpflichtung der Tarifkommission hinzuwirken. [In der Krankenhausbewegung wurde demokratische Mitbestimmung geprobt]({{< relref "20230608151937-in_der_krankenhausbewegung_wurde_demokratische_mitbestimmung_geprobt.md" >}}), hieran können wir uns orientieren.
