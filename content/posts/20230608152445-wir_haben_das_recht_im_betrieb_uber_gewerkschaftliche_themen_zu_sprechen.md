+++
title = "Wir haben das Recht im Betrieb über gewerkschaftliche Themen zu sprechen"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

Es ist erlaubt im Betrieb und auch während der Arbeitszeit über gewerkschaftliche Themen zu sprechen oder Gewerkschaftsarbeit zu machen. Bedingung: man stört bzw. behindert den Arbeitsablauf nicht.
