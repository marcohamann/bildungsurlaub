+++
title = "Wegezeiten sind ein Hot-Topic im Betrieb"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Betrieb"]
draft = false
+++

Die langen und unbezahlten Wegezeiten sind ein _hot-topic_ im Betrieb. Das machen andere Betriebe anscheinend besser. [Die Ausgestaltung der Arbeitszeit liegt beim Betriebsrat]({{< relref "20230607180107-die_ausgestaltung_der_arbeitszeit_liegt_beim_betriebsrat.md" >}})


## <span class="org-todo todo TODO">TODO</span> konkrete Beispiel {#konkrete-beispiel}
