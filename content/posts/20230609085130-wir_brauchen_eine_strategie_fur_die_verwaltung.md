+++
title = "Wir brauchen eine Strategie für die Verwaltung."
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Betrieb"]
draft = false
+++

Uns fehlt eine Strategie für die Verwaltung. Wie organisieren wir die Kollegen dort? Wie kann in der Verwaltung gestreikt werden?

Was sind die _hot-topics_ in der Verwaltung? Was sind Hemmnisse für die Organisation? Was sind Hemmnisse für den Streik?
