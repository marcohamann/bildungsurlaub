+++
title = "Vorbereitungszeit könnte ein Ansatz für kollektive Aktion sein"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Betrieb"]
draft = false
+++

Die Vorbereitung für die Fahrer impliziert die Konsultation des Meggi. Die Erfüllung der Arbeitsleistung setzt diese Vorbereitung voraus, es muss sich beispielsweise über Baustellen und Umleitungen informiert werden. Die vorgesehene Zeit reicht nicht aus. Ein Problem ist, dass laut Betriebsrat der Test der Vorbereitungszeit ergeben hat, dass die Vorbereitungszeit ausgereicht hätte.

Dies könnte ein Ansatzpunkt für kollektive Aktion sein, weil es offenbar viele Kollegen ärgert und betrifft. Kollektive Aktion hieße Dienst nach Vorschrift und/oder Gefährdungsanzeigen kollektiv zu organisieren. Auch [Die Wendezeit könnte ein Ansatzpunkt für kollektive Aktion sein.]({{< relref "20230608111123-die_wendezeit_konnte_ein_ansatzpunkt_fur_kollektive_aktion_sein.md" >}}) Möglicherweise relevant, der Umstand, dass viele Kollegen in Haustarifverträgen sind: [Leobus und LSVB sind nicht im TV-N]({{< relref "20230607175729-leobus_und_lsvb_sind_nicht_im_tv_n.md" >}})

Zu beachten: [Die Ausgestaltung der Arbeitszeit liegt beim Betriebsrat]({{< relref "20230607180107-die_ausgestaltung_der_arbeitszeit_liegt_beim_betriebsrat.md" >}}).
