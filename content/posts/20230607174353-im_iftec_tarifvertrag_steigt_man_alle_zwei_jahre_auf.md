+++
title = "Im Iftec Tarifvertrag steigt man alle zwei Jahre auf"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Iftec", "Betrieb"]
draft = false
+++

Der Tarifvertrag der Iftec sieht einen Stufenanstieg alle zwei Jahre vor. Das senkt die Attraktivität des TV-N für die Kollegen (Stufenaufstieg alle 5 Jahre).
