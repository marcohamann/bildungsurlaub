+++
title = "Die LVB kann mit der LSB in den Solidaritätsstreik treten."
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "LSB", "Betrieb"]
draft = false
+++

Durch die [wirtschaftliche Verflechtung]({{< relref "20230608114541-wirtschaftliche_verflechtung_ist_grundlage_fur_solidaritatsstreik.md" >}}) (LSB arbeitet zu 80% für die LVB) und hat [personelle Verflechtungen]({{< relref "20230608114650-personelle_verflechtungen_begrundet_solidaritatsstreiks.md" >}}) (mind. ein Geschäftsführer der LSB ist Abteilungs(?)leiter bei der LVB) kann die LVB mit der LSV solidarisch streiken.
Dabei gilt es zu beachten: [Solidaritätsstreiks müssen verhältnismäßig sein]({{< relref "20230608114306-solidaritatsstreiks_mussen_verhaltnismaßig_sein.md" >}}).
