+++
title = "wirtschaftliche Verflechtung ist Grundlage für Solidaritätsstreik"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

Wirtschaftliche Verflechtung zwischen zwei Unternehmen begründet die Möglichkeit für Solidaritätsstreiks.
