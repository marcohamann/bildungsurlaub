+++
title = "Der Bildungsurlaub kann übertragen werden"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Betrieb"]
draft = false
+++

Der Anspruch auf Bildungsurlaub kann auf das nächste Jahr übertragen werden. Dafür muss ein begründeter Antrag gestellt werden. Begründung kann z.B. sein, dass der Bildungsurlaub aufgrund betrieblicher Gründe nicht wahrgenommen werden konnte. [In betrieblichen Angelegenheiten gilt eine Ausschlussfrist]({{< relref "20230607192209-in_betrieblichen_angelegenheiten_gilt_eine_ausschlussfrist.md" >}})

[In der LVB gibt es Urlaubsspitzen und -flauten]({{< relref "20230607192537-in_der_lvb_gibt_es_urlaubsspitzen_und_flauten.md" >}}) dies gilt es bei der Planung eines Bildungsurlaubs zu beachten.
