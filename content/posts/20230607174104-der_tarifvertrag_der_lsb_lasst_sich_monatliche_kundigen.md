+++
title = "Der Tarifvertrag der LSB lässt sich monatliche kündigen"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "LSB", "Betrieb"]
draft = false
+++

Der Mantel-Tarifvertrag der LSB lässt sich monatlich kündigen, der Entgelt-Tarifvertrag läuft bis Ende 2024.
