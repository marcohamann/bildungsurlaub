+++
title = "In der LVB gibt es Urlaubsspitzen und -flauten"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Betrieb"]
draft = false
+++

In der LVB gibt es Urlaubsspitzen, die vor allem beim Fahrpersonal zu Engpässen führen. Flauten gibt es laut Aussage von JHK (BR) im Januar, im Februar rund um die Ferien und im November.
