+++
title = "Wir müssen unsere Tarifziele in der LVB bestimmen"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Leobus", "LSB", "LSVB", "Iftec", "LAB", "Betrieb"]
draft = false
+++

Wir haben unterschiedliche Tarifverträge ([Leobus und LSVB sind nicht im TV-N]({{< relref "20230607175729-leobus_und_lsvb_sind_nicht_im_tv_n.md" >}}), genausowenig die Iftec und die LSB), unterschiedliche Probleme, Bedürfnisse und [Organisationsgrad bei der LVB]({{< relref "20230607173217-organisationsgrad_bei_der_lvb.md" >}}). Langfristig sollten wir Tarifziele definieren.

Eine Zielgerade könnte Ende 2024 sein, zu diesem Zeitpunkt laufen die Verträge von LVB, Leobus, LSVB, LAB, LTB aus. Die Iftec müsste versuchen ihre Verträge auf diesen Zeitpunkt hin auszurichten.
