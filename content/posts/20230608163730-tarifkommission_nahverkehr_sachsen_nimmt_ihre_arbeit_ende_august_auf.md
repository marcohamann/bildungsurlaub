+++
title = "Tarifkommission Nahverkehr Sachsen nimmt ihre Arbeit Ende August auf"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Sachsen", "Betrieb"]
draft = false
+++

Gegen Ende August wird die sächsische Tarifkommission zusammenkommen, um den Fragebogen zur Mitgliederbefragung fertig zu machen. Bis Ende September muss der Tarifvertrag gekündigt werden.
