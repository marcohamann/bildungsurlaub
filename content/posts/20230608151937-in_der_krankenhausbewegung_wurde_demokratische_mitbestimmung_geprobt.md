+++
title = "In der Krankenhausbewegung wurde demokratische Mitbestimmung geprobt"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

In der Krankenhausbewegung wurde Mitbestimmung von unten erfolgreich erprobt. [Die Tarifkommission kann alleine entscheiden]({{< relref "20230608152103-die_tarifkommission_kann_alleine_entscheiden.md" >}}). Aber durch die Wahl von Streikdelegierten wurden die Verhandlungen in die Öffentlichkeit gezerrt: die Tarif- und Verhandlungskommission hat die Angebote der Arbeitgeber immer rückgesprochen und dadurch die Verhandlung quasi öffentlich geführt.
