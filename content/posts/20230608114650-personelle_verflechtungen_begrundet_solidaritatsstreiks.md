+++
title = "Personelle Verflechtungen begründet Solidaritätsstreiks"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

Personelle Verflechtungen zwischen zwei Unternehmen begründet die Möglichkeit für Solidaritätsstreiks.
