+++
title = "Was sind unsere Rechte bezüglich der Betriebsversammlung?"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Betrieb"]
draft = false
+++

Aufgrund der Einschränkungen, die uns der Betriebsrat bezüglich unserer Rede bei der Betriebsversammlung auferlegt, müssen wir uns kundig machen: was haben wir überhaupt für Rechte auf einer Betriebsversammlung? Was können wir rechtlich durchsetzen, wo kommt es darauf an, die Spielräume durch Absprachen/Kompromisse zu erhalten?
