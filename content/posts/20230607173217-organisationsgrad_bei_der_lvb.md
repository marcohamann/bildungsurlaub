+++
title = "Organisationsgrad bei der LVB"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Betrieb"]
draft = false
+++

Bei der LVB sind ca. 900 Verdi-Mitglieder, Stand Juni 2023. Vor der Tarifverhandlung ÖD 2023 waren es ca. 580 Mitglieder. Insgesamt arbeiten 2.000 Kollegen bei der LVB inklusive Leobus, LSVB und LAB. Im Fahrdienst arbeiten 1200 Kollegen: [Leobus und LSVB sind nicht im TV-N]({{< relref "20230607175729-leobus_und_lsvb_sind_nicht_im_tv_n.md" >}}). Dazu kommen Iftec mit 500 Kollegen, die LSB mit knapp 200 Kollegen und die LTB mit 20 Kollegen. Insgesamt umfasst die LVB-Gruppe also 2.700 Kollegen.


## <span class="org-todo todo TODO">TODO</span> Wie verteilen sich die Mitglieder über die verschiedenen Betriebsteile? {#wie-verteilen-sich-die-mitglieder-über-die-verschiedenen-betriebsteile}
