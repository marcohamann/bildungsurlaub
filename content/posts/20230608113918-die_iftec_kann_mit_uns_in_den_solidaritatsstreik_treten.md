+++
title = "Die Iftec kann mit uns in den Solidaritätsstreik treten"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Iftec", "Betrieb"]
draft = false
+++

Die Iftec zweigt ihre Gewinne an die LVB ab und ist somit mit der LVB wirtschaftlich verbunden: [wirtschaftliche Verflechtung ist Grundlage für Solidaritätsstreik]({{< relref "20230608114541-wirtschaftliche_verflechtung_ist_grundlage_fur_solidaritatsstreik.md" >}}). Auch [Die LVB kann mit der LSB in den Solidaritätsstreik treten.]({{< relref "20230608114046-die_lvb_kann_mit_der_lsb_in_den_solidaritatsstreik_treten.md" >}})
