+++
title = "In betrieblichen Angelegenheiten gilt eine Ausschlussfrist"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

In betrieblichen Belangen gilt eine Ausschlussfrist. Ansprüche auf tarifliche (und gesetzliche?) Vereinbarungen müssen innerhalb dieser Frist geltend gemacht werden, sonst verfallen sie.
