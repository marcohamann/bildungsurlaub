+++
title = "Wie bauen wir bei der LVB unsere Struktur langfristig auf"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Betrieb"]
draft = false
+++

Welche langfristige Strukturen brauchen wir in der LVB? Ansätze finden sich in Halle: [Die Betriebsgruppe in Halle ist außerordentlich gut organisiert]({{< relref "20230608172238-die_betriebsgruppe_in_halle_ist_außerordentlich_gut_organisiert.md" >}}) und [Die Betriebsgruppe in Halle hat ihre Strukturen und Abläufe dokumentiert]({{< relref "20230608173637-die_betriebsgruppe_in_halle_hat_ihre_strukturen_und_ablaufe_dokumentiert.md" >}}).
