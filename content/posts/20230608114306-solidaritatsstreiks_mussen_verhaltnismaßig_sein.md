+++
title = "Solidaritätsstreiks müssen verhältnismäßig sein"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

Solidaritätsstreik müssen verhältnismäßig sein. Bei Verdi wird angepeilt das maximal 40% der im Hauptbetrieb Streikenden als Unterstützer in den Streik treten.
