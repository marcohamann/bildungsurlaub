+++
title = "Die Wendezeit könnte ein Ansatzpunkt für kollektive Aktion sein."
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Betrieb"]
draft = false
+++

Die Wendezeiten sind für viele Fahrer ein Ärgernis, weil sie bzw. die Umlaufzeiten zu kurz berechnet sind. Problem ist, dass Kollegen die Strecken schneller fahren und dadurch in der Statistik den Eindruck erzeugen, dass die Zeit ausreicht.
Hier wäre ein Ansatzpunkt Verspätungen durch kollektiven Dienst nach Vorschrift zu erzeugen. Auch [Vorbereitungszeit könnte ein Ansatz für kollektive Aktion sein]({{< relref "20230608110638-vorbereitungszeit_konnte_ein_ansatz_fur_kollektive_aktion_sein.md" >}}).

Zu beachten: [Die Ausgestaltung der Arbeitszeit liegt beim Betriebsrat]({{< relref "20230607180107-die_ausgestaltung_der_arbeitszeit_liegt_beim_betriebsrat.md" >}})
