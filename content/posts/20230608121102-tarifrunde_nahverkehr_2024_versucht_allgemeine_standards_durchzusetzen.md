+++
title = "Tarifrunde Nahverkehr 2024 versucht allgemeine Standards durchzusetzen"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Nahverkehr", "Betrieb"]
draft = false
+++

In der diesjährigen Tarifrunde Nahverkehr wird von Verdi angestrebt bestimmte allgemeine Mindeststandards in allen Verkehrsunternehmen durchzusetzen. Das Ziel eines Flächentarifvertrags wurde aufgegeben (vorerst). Verhandlungsführer von Verdi ist rumgreist um alle Tarifkommissionen einzuschwören. Bayern aber schon eingeknickt. [Wir müssen unsere Tarifziele in der LVB bestimmen]({{< relref "20230608113614-wir_mussen_unsere_tarifziele_in_der_lvb_bestimmen.md" >}})
