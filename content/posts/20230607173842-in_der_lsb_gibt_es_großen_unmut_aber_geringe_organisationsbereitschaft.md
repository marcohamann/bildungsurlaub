+++
title = "In der LSB gibt es großen Unmut, aber geringe Organisationsbereitschaft"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "LSB", "Betrieb"]
draft = false
+++

Die Kollegen von der LSB werden nicht nach TV-N bezahlt und sind mit ihren Arbeitsbedingungen sehr unzufrieden. Sie fanden den LVB/ÖD-Streik gut: "Geil was die da machen!" Aber es gibt kaum Bereitschaft sich zur Organisieren. Gleichzeitig fand ein offener Brief 130 Unterzeichner, von insgesamt 190 Kollegen.
Eine Möglichkeit gemeinsam zu streiken und zu mobilisieren, wäre es den Tarifvertrag der LSB auch zu Beginn des Jahres zu kündigen: [Der Tarifvertrag der LSB lässt sich monatliche kündigen]({{< relref "20230607174104-der_tarifvertrag_der_lsb_lasst_sich_monatliche_kundigen.md" >}})
