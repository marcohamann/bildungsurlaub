+++
title = "Die Bundestarifkommission ist schlecht vernetzt"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

Die Mitglieder der Bundestarifkommission im Öffentlichen Dienst ist schlecht vernetzt. Die Kollegen kennen sich nicht, haben keine Kommunikationskanäle, über die sie sich austauschen könnten.
