+++
title = "Bei falscher Eingruppierung muss Arbeitnehmer dagegen vorgehen"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb", "LVB"]
draft = false
+++

Die Eingruppierung bzw. Bezahlung richtet sich nach der ausgeübten Tätigkeit. Die Eingruppierung kontrolliert der Betriebsrat. Sollte der Betriebsrat gegen eine (falsche) Eingruppierung nichts machen, muss der Beschäftigte selbst bzw. mit Unterstützung der Gewerkschaft dagegen klagen.
