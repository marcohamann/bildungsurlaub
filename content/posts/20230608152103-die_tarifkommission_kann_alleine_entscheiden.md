+++
title = "Die Tarifkommission kann alleine entscheiden"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

Die Tarifkommission ist nach Verdi-Satzung souverän und kann alleine über Annahme und Ablehnung von Angeboten in Tarifverhandlungen entscheiden. Eine demokratische Kontrolle ist nicht vorgesehen.[Die Tarifkommission kann man nur durch Vereinbarung binden]({{< relref "20230608163420-die_tarifkommission_kann_man_nur_durch_vereinbarung_binden.md" >}}).
