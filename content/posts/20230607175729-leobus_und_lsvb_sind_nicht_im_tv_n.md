+++
title = "Leobus und LSVB sind nicht im TV-N"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "LVB", "Leobus", "LSVB", "Betrieb"]
draft = false
+++

Leobus und LSVB sind nicht im TV-N, bei ihnen findet nur der TV-N Anwendung. Das hat Auswirkungen wie z.B.: es gelten nur die Tabellen, aber nicht die Regelung zum TVÖD. Insofern ist die Übertragung der TVÖD-Ergebnisse nicht rechtlich abgesichert und muss verhandelt werden.
