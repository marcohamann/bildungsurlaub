+++
title = "Die Betriebsgruppe in Halle ist außerordentlich gut organisiert"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Havag", "Betrieb"]
draft = false
+++

Die Kollegen aus Halle haben eine funktionierende, vorbildliche Betriebsgruppen-Strukur. Die bildet sich wie folgt: aus den verschiedenen Betriebsteilen werden Vertrauensleute gewählt. Aus dem Kreis der Vertrauensleute wird eine Vertrauensleute-Leitung gewählt. Diese bereitet in offenen Treffen monatliche Diskussionsabende vor, bei denen in lockerer Runde bestimmte Themen vorgestellt und diskutiert werden. Außerdem bildet die Leitung auch die Arbeitskampfleitung. [Die Betriebsgruppe in Halle hat ihre Strukturen und Abläufe dokumentiert]({{< relref "20230608173637-die_betriebsgruppe_in_halle_hat_ihre_strukturen_und_ablaufe_dokumentiert.md" >}})
