+++
title = "Die Ausgestaltung der Arbeitszeit liegt beim Betriebsrat"
author = ["Marco Hamann"]
tags = ["Gewerkschaft", "Betrieb"]
draft = false
+++

Arbeitszeit wird grundlegend geregelt durch das Arbeitszeitgesetz. Tarifverträge können Abweichungen (nach unten) regeln. Dabei geben Tarifverträge in erster Linie den Rahmen vor, die konkrete Ausgestaltung obliegt dem Betriebsrat.
